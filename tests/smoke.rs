#![feature(plugin)]
#![plugin(output2)]
#![plugin(output3)]

#[test]
fn works() {
     assert_eq!(2, output2!(TEST2));;
     assert_eq!(3, output3!(TEST3));;
}