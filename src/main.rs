extern crate libloading as lib;

use libloading::Library;
use std::env;
use std::fs;
use std::process::Command;


fn main() {



  let args: Vec<String> = env::args().collect();
  

let code = format!("//#![crate_type=\"dylib\"]
#[no_mangle]
fn SUM(n:f64) -> f64 {{println!(\"{{:?}}\", {}+{}); n }}", args[1], args[2]);




     fs::write("plugin-output2/src/output2.rs", code).expect("Unable to write file");


let _output = if cfg!(target_os = "windows") {
    Command::new("cmd")
            .args(&["/C", "cargo build --manifest-path=plugin-output2/Cargo.toml"])
            .output()
            .expect("failed to execute process")
} else {
    Command::new("sh")
            .arg("-c")
            .arg("cargo build --manifest-path=plugin-output2/Cargo.toml")
            .output()
            .expect("failed to execute process")
};



    println!("TEST");
    
let lib = if cfg!(target_os = "windows") {
    Library::new("plugin-output2/target/debug/output2.dll").unwrap()
} else {
    Library::new("plugin-output2/target/debug/liboutput2.so").unwrap()
};



 println!("TEST2");
unsafe {
    let SUM: lib::Symbol<unsafe extern fn(f64) -> f64> =
        lib.get(b"SUM\0").unwrap();
    SUM(0.42);
}


  //  println!("{}", output3!(TEST2));
}